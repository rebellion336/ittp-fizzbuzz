export const fizzbuzz = (x) =>{
    if(x%3 === 0 && x%5 === 0){
        return 'fizzbuzz'
    } else if(x%3 === 0){
        return 'fizz'
    } else if(x%5 === 0){
        return 'buzz'
    } else if(typeof x !== 'number'){
        throw Error('x is not a number')
    } else if(typeof x === 'number'){
        return x
    }
}

export const returnReadWord = (x)=>{
    switch (x) {
        case 1:{
            return 'หนึ่ง'
        }
        case 2:{
            return 'สอง'
        }
        case 3:{
            return 'สาม'
        }
        case 4:{
            return 'สี่'
        }
        case 5:{
            return 'ห้า'
        }
        case 6:{
            return 'หก'
        }
        case 7:{
            return 'เจ็ด'
        }
        case 8:{
            return 'แปด'
        }
        case 9:{
            return 'เก้า'
        }
        case 10:{
            return 'สิบ'
        }
        
    } 

}