import {fizzbuzz, returnReadWord} from '../fizzbuzz'

describe('fizzbuzz',()=>{
    it('number is 15',()=>{
        expect(fizzbuzz(15)).toBe('fizzbuzz')
    })
    it('number is 3',()=>{
        expect(fizzbuzz(3)).toBe('fizz')
    })
    it('number is 5',()=>{
        expect(fizzbuzz(5)).toBe('buzz')
    })
    it('input is not a number to throw error',()=>{
        expect(()=>{
            fizzbuzz(a)
        }).toThrow()
    })
    it('inpet is number but not cant mod 3 ro 5',()=>{
        expect(fizzbuzz(2)).toBe(2)
    })
})

describe('return read word',()=>{
    it('number = 1', () => {
        expect(returnReadWord(1)).toBe('หนึ่ง')
    })
    it('number = 2', () => {
        expect(returnReadWord(2)).toBe('สอง')
    })
    it('number = 3', () => {
        expect(returnReadWord(3)).toBe('สาม')
    })
    it('number = 4', () => {
        expect(returnReadWord(4)).toBe('สี่')
    })
    it('number = 5', () => {
        expect(returnReadWord(5)).toBe('ห้า')
    })
    it('number = 6', () => {
        expect(returnReadWord(6)).toBe('หก')
    })
    it('number = 7', () => {
        expect(returnReadWord(7)).toBe('เจ็ด')
    })
    it('number = 8', () => {
        expect(returnReadWord(8)).toBe('แปด')
    })
    it('number = 9', () => {
        expect(returnReadWord(9)).toBe('เก้า')
    })
    it('number = 10', () => {
        expect(returnReadWord(10)).toBe('สิบ')
    })
})